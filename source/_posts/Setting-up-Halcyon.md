---
title: Setting up Halcyon
date: 2018-06-20 18:04:25
tags:
---

Congratulations on finding the best Hexo theme!

To get started, either [download a .zip](https://gitlab.com/Krakob/hexo-theme-halcyon/-/archive/master/hexo-theme-halcyon-master.zip)
of the theme, or clone `git@gitlab.com:Krakob/hexo-theme-halcyon.git` (ssh) / `https://gitlab.com/Krakob/hexo-theme-halcyon.git` (https).
Put the theme in `your_hexo_blog/themes/halcyon` and change the `theme` parameter in `_config.yml` to `halcyon`.

You'll also need to install the following npm packages:
```
hexo-renderer-pug
hexo-renderer-stylus
```

Install by running `npm install --save hexo-renderer-pug hexo-renderer-stylus`.

You should now be able to build your blog with `hexo generate`!

The theme should be compatible with the default blog spawned by `hexo init` per [official documentation](https://hexo.io/docs/setup.html),
but to get the most out of Halcyon, make sure to set the theme's [custom settings](#fix-me!).

If you run into any issues, feel free to post to the [issue tracker](https://gitlab.com/Krakob/hexo-theme-halcyon/issues).
